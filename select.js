"use strict";

class YTools {
    static isElement(obj) {
        try {
            return obj instanceof HTMLElement;
        } catch (e) {
            return (typeof obj === "object") && (obj.nodeType === 1) && (typeof obj.style === "object") && (typeof obj.ownerDocument === "object");
        }
    }

    /**
     * @source https://gomakethings.com/vanilla-javascript-version-of-jquery-extend/
     */
    static extend() {
        let extended = {},
            deep = false,
            i = 0,
            length = arguments.length;

        if (Object.prototype.toString.call(arguments[0]) === "[object Boolean]") {
            deep = arguments[0];
            i++;
        }

        let merge = obj => {
            for (let prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    if (deep && Object.prototype.toString.call(obj[prop]) === "[object Object]") {
                        extended[prop] = this.extend(true, extended[prop], obj[prop]);
                    } else {
                        extended[prop] = obj[prop];
                    }
                }
            }
        };

        for (; i < length; i++) {
            merge(arguments[i]);
        }

        return extended;
    }

    static ready(fn) {
        let state = document.readyState === "interactive" || document.readyState === "complete";

        if(!state) {
            document.onreadystatechange = () => {
                this.ready(fn);
            };

            return false;
        }

        if(typeof fn === "function") {
            fn();
        }

        return state;
    }

    static setDetectChangeHandler(field) {
        if(!field.handlerInstalled) {
            let superProps = Object.getPrototypeOf(field),
                superSet = Object.getOwnPropertyDescriptor(superProps, "value").set,
                superGet = Object.getOwnPropertyDescriptor(superProps, "value").get,
                newProps = {
                    get: function () {
                        return superGet.apply(this, arguments);
                    },
                    set: function (t) {
                        setTimeout(() => {
                            this.dispatchEvent(new Event("yChange"));
                        }, 50);
                        return superSet.apply(this, arguments);
                    }
                };

            Object.defineProperty(field, "value", newProps);

            field.handlerInstalled = true;
        }
    }

    static collectionToArray(collection) {
        return [].slice.call(collection);
    }

    static arrayUnique(array) {
        return array.filter((elem, pos, arr) => arr.indexOf(elem) === pos);
    }
}

class YSelect {
    /**
     * @var jQuery
     */
    constructor() {
        const _this = this;

        this.items = [];
        this.keys = {
            ENTER: 13,
            ESCAPE: 27,
            SPACE: 32,
            UP: 38,
            DOWN: 40,
            CTRL: 17
        };
        this.observerParams = {
            attributes: true,
            childList: true,
            subtree: true
        };
        this.states = {
            CTRL_ACTIVE: false
        };

        // Инициализация функции на DOM элементах
        Object.prototype.ySelect = function (params) {
            YTools.ready(() => _this.init(this, params));
        };

        // Инициализация функции на jQuery элементах
        if(typeof jQuery !== "undefined") {
            jQuery.fn.ySelect = function (params) {
                YTools.ready(() => _this.init(jQuery(this).toArray(), params));
            };
        }

        this.eventListener();
    }

    eventListener() {
        const keyState = e => {
            let state = e.type.toLowerCase() === "keydown";

            // CTRL
            if(e.keyCode === this.keys.CTRL) {
                this.states.CTRL_ACTIVE = state;
            }

            // Up/Down
            if(state) {
                switch (e.keyCode) {
                    case this.keys.UP:
                        // TODO

                        break;
                    case this.keys.DOWN:
                        // TODO

                        break;
                    case this.keys.ESCAPE:
                        // TODO

                        break;
                    case this.keys.ENTER:
                    case this.keys.SPACE:
                        // TODO

                        break;
                }
            }
        };

        document.addEventListener("keydown", keyState);
        document.addEventListener("keyup", keyState);

        // Outside click closing all selects
        window.addEventListener("click", () => this.items.forEach(item => this.close(item)));
    }

    /**
     * @param node
     * @param params
     * @returns {{params: *}}
     */
    init(node, params) {
        this.lockedListenOptionAttrs = ["selected", "disabled"];
        const restrictedListenOptionAttrs = ["class", "width", "height", "style", "id"];
        const defaultParams = {
            type: "select", // "select" or "input"
            placeholder: null,
            textOverflow: "clip", // "clip" or "ellipsis"
            search: false,
            searchMode: "strict", // "strict" or "flexible" ("flexible" mode requires a plugin "rmm5t/liquidmetal")
            multipleWithCtrl: false,
            multipleGlue: ", ",
            keyboard: true,
            liveReload: true,
            showCallback: null,
            hideCallback: null,
            listenOptionAttrs: [], // Attributes list of option tag, that will be tracked and moved to the new select
        };

        if (typeof params !== "object") {
            params = {};
        }

        params = YTools.extend(defaultParams, params, {listenOptionAttrs: this.lockedListenOptionAttrs});

        restrictedListenOptionAttrs.forEach(attr => {
            if(params[attr]) {
                delete params["attr"];
            }
        });

        if (YTools.isElement(node)) {
            this.prepareItem(node, params);
        } else if (node instanceof NodeList || Array.isArray(node)) {
            node.forEach(item => this.prepareItem(item, params));
        } else {
            console.warn("YSelect: present object is not a DOM element", node);
        }
    }

    /**
     * @param item
     * @returns {boolean}
     */
    checkItem(item) {
        // Checking type of element
        if (item.tagName !== "SELECT") {
            console.warn("YSelect: present object is not a Select node", item);
            return false;
        }

        // Check is already initialized
        if(this.items.indexOf(item) >= 0) {
            return false;
        }

        return true;
    }

    /**
     * @param item
     * @param params
     */
    prepareItem(item, params) {
        if (!this.checkItem(item)) {
            return ;
        }

        this.items.push(item);

        item.ySelectParams = {
            multiple: item.getAttribute("multiple") !== null,
            params: params,
            options: [],
            container: null
        };

        item.ySelectParams.container = this.build(item);

        this.draw(item);
        this.listenItem(item);

        Object.freeze(item.ySelectParams);
    }

    collectOptions(select) {
        if(select && select.children) {
            for (let i = 0; i < select.children.length; i++) {
                let option = select.children[i],
                    optionAttributes = {};

                select.ySelectParams.params.listenOptionAttrs.forEach(key => {
                    if(option.attributes[key]) {
                        optionAttributes[key] = option.attributes[key];
                    }
                });

                let optionData = {
                        text: option.innerText,
                        value: option.value,
                        params: {
                            selected: option.selected,
                            disabled: option.disabled,
                        },
                        attributes: optionAttributes,
                        element: option,
                        containerElement: null
                    };

                select.ySelectParams.options.splice(option.index, 0, optionData);
            }
        }
    }

    /**
     * @param select
     */
    build(select) {
        this.collectOptions(select);

        // Creating structure
        let container = document.createElement("div"),
            selectContainer = document.createElement("div"),
            customSelect = document.createElement("div"),
            selectText = document.createElement("div"),
            selectArrow = document.createElement("div"),
            selectDrop = document.createElement("div"),
            selectOptions = document.createElement("ul");

        container.classList.add("yselect", "yselect__container");
        selectContainer.classList.add("yselect__hidden");
        customSelect.classList.add("yselect__select");
        selectText.classList.add("yselect__select-text");
        selectArrow.classList.add("yselect__select-arrow");
        selectDrop.classList.add("yselect__drop-container");
        selectOptions.classList.add("yselect__options-list");

        if(select.ySelectParams.params.textOverflow === "ellipsis") {
            selectText.classList.add("yselect__select-text--ellipsis");
        }

        customSelect.append(selectText, selectArrow);
        selectDrop.append(selectOptions);
        container.append(selectContainer, customSelect, selectDrop);

        select.parentElement.insertBefore(container, select);

        selectContainer.appendChild(select);

        // Adding event listeners
        customSelect.addEventListener("click", e => {
            e.stopPropagation();

            this.toggle(select);
        });

        return container;
    }

    /**
     * @param select
     * @param name
     * @param value
     * @param params
     * @param index
     * @param observed
     */
    insertOption(select, name, value, params, index, observed) {
        let option = null;

        if(!observed) {
            option = document.createElement("option");
            option.innerText = name;
            option.value = value;
            option.selected = params.selected;
            option.disabled = params.disabled;
            option._selectIndex = index;

            select.ySelectParams.options[index].element = option;

            select.insertBefore(option, select.children[index]);
        } else {
            option = select.children[index];
        }

        select.ySelectParams.options.splice(index, 0, {
            text: name,
            value: value,
            params: params,
            attributes: {},
            element: option,
            containerElement: null
        });

        select.ySelectParams.options.forEach((option, index) => {
            if(option.element) {
                option.element._selectIndex = index;
            }
            if(option.containerElement) {
                option.containerElement._selectIndex = index;
            }
        });

        this.draw(select);
    }

    /**
     * @param select
     * @param index
     * @param observed
     */
    removeOption(select, index, observed) {
        let option = select.ySelectParams.options[index];
        if(option && option.containerElement) {
            option.containerElement.remove();
        }
        select.ySelectParams.options.splice(index, 1);

        select.ySelectParams.options.forEach((option, newIndex) => {
            if(option.element) {
                option.element._selectIndex = newIndex;
            }
            if(option.containerElement) {
                option.containerElement._selectIndex = newIndex;
            }
        });

        if(!observed) {
            select.children[index].remove();
        }

        this.draw(select);
    }

    select(select, value, fromEvent, skipValue) {
        let selectTextItem =  select.ySelectParams.container.querySelector(".yselect__select-text"),
            multipleMode = select.ySelectParams.multiple,
            multipleActive = !(select.ySelectParams.params.multipleWithCtrl ^ this.states.CTRL_ACTIVE); // XOR

        console.log(value);

        const selectItem = item => {
            if(item.params.selected && multipleMode && multipleActive) {
                item.element.removeAttribute("selected");
                item.element.selected = false;
                item.params.selected = false;
                item.containerElement.classList.remove("yselect__option--selected");
            } else {
                if (!(multipleMode && multipleActive)) {
                    let siblings = item.element.parentNode.children;
                    for (let i = 0; i < siblings.length; i++) {
                        siblings[i].removeAttribute("selected");
                        siblings[i].selected = false;
                        select.ySelectParams.options[i].params.selected = false;
                    }
                }

                item.element.setAttribute("selected", "selected");
                item.element.selected = true;
                item.params.selected = true;

                if (item.containerElement) {
                    let containerSiblings = item.containerElement.parentNode.children;

                    if (!(multipleMode && multipleActive)) {
                        for (let i = 0; i < containerSiblings.length; i++) {
                            containerSiblings[i].classList.remove("yselect__option--selected");
                        }
                    }

                    item.containerElement.classList.add("yselect__option--selected");
                }

                if (!((multipleMode && multipleActive) || skipValue)) {
                    select.value = value;
                }
            }

            let text = [];

            select.ySelectParams.options.forEach(item => {
                if(item.params.selected) {
                    text.push(item.text);
                }
            });

            selectTextItem.innerText = text.length ? text.join(select.ySelectParams.params.multipleGlue) : select.ySelectParams.params.placeholder;
        };

        if(Array.isArray(value)) {

            if(multipleMode) {
                let ctrlState = this.states.CTRL_ACTIVE;
                this.states.CTRL_ACTIVE = true;

                value.forEach(arValue => {
                    this.select(select, arValue, fromEvent, skipValue);
                });

                this.states.CTRL_ACTIVE = ctrlState;
            } else {
                this.select(select, value[value.length - 1], fromEvent, skipValue);
            }

        } else if(value instanceof NodeList) {

            let ctrlState = this.states.CTRL_ACTIVE;
            this.states.CTRL_ACTIVE = true;

            value.forEach(item => {
                let index = item.index;//YTools.collectionToArray(item.parentNode.children).indexOf(item);

                selectItem(select.ySelectParams.options[index]);
            });

            this.states.CTRL_ACTIVE = ctrlState;

        } else if((value instanceof HTMLElement || value instanceof Node) && value.tagName === "OPTION") {

            let index = value.index;//YTools.collectionToArray(value.parentNode.children).indexOf(value);

            selectItem(select.ySelectParams.options[index]);

        } else if(typeof value === "string" || typeof value === "number") {

            let valueFound = false;

            select.ySelectParams.options.forEach(item => {
                if(!valueFound && item.value && item.element && !item.params.disabled && item.value == value) {
                    selectItem(item);
                }
            });

        } else {
            selectItem(value);
        }
    }

    /**
     * @param select
     */
    draw(select) {
        if(select.ySelectParams.observer) {
            select.ySelectParams.observer.disconnect();
        }

        const buildOption = (optionData, optionIndex) => {
            let option = document.createElement("li");
            option.classList.add("yselect__option");

            if(optionData.params.disabled) {
                option.classList.add("yselect__option--disabled");
            }
            if(optionData.params.selected) {
                option.classList.add("yselect__option--selected");
            }

            Object.keys(optionData.attributes).forEach(name => {
                if(this.lockedListenOptionAttrs.indexOf(name) < 0) {
                    option.setAttribute(name, optionData.attributes[name]);
                }
            });

            option.innerText = optionData.text;
            option._optionIndex = optionIndex;

            return option;
        };

        let container = select.ySelectParams.container,
            placeholder = select.ySelectParams.params.placeholder,
            selectText = container.querySelector(".yselect__select-text"),
            optionsContainer = container.querySelector(".yselect__options-list"),
            selectedTextAr = [];

        select.ySelectParams.options.forEach((option, index) => {
            if(option.containerElement) {
                option.containerElement.remove();
            }

            if(option.element) {
                option.element._selectIndex = index;

                option.element.disabled = option.params.disabled;
                option.element.selected = option.params.selected;

                if(option.element.selected) {
                    selectedTextAr.push(option.text);
                }
            }

            let newOption = buildOption(option);
            option.containerElement = newOption;
            optionsContainer.appendChild(newOption);

            newOption.addEventListener("click", e => {
                e.stopPropagation();

                if(!option.params.disabled) {
                    this.select(select, option, true);

                    if (select.ySelectParams.multiple) {
                        if (select.ySelectParams.params.multipleWithCtrl) {
                            if (!this.states.CTRL_ACTIVE) {
                                this.close(select);
                            }
                        }
                    } else {
                        this.close(select);
                    }
                }
            });

            option.containerElement.classList.toggle("yselect__option--disabled", option.params.disabled);
            option.containerElement.classList.toggle("yselect__option--selected", option.params.selected);
        });

        selectText.innerText = !selectedTextAr.length && placeholder ? placeholder : selectedTextAr.join(select.ySelectParams.params.multipleGlue);

        if(select.ySelectParams.observer) {
            select.ySelectParams.observer.observe(select, this.observerParams);
        }
    }

    toggle(select) {
        let isOpen = select.ySelectParams.container.classList.contains("yselect--open"),
            callback = isOpen ? "closeCallback" : "openCallback";

        select.ySelectParams.container.classList.toggle("yselect--open", !isOpen);

        if(select.ySelectParams.params[callback] && typeof select.ySelectParams.params[callback] === "function") {
            select.ySelectParams.params[callback]();
        }
    }

    open(select) {
        let callback = "openCallback";
        select.ySelectParams.container.classList.add("yselect--open");

        if(select.ySelectParams.params[callback] && typeof select.ySelectParams.params[callback] === "function") {
            select.ySelectParams.params[callback]();
        }
    }

    close(select) {
        let callback = "closeCallback";
        select.ySelectParams.container.classList.remove("yselect--open");

        if(select.ySelectParams.params[callback] && typeof select.ySelectParams.params[callback] === "function") {
            select.ySelectParams.params[callback]();
        }
    }

    search(select, query) {
        // TODO: implement method
    }

    /**
     * @param select
     */
    listenItem(select) {
        // Listening changes by default event listener
        const change = e => {
            let values = [];

            YTools.collectionToArray(e.target.children).forEach(option => {
                if(option.selected) {
                    values.push(option.value);
                }
            });

            if(e.target.value) {
                values.push(e.target.value);
            }

            values = YTools.arrayUnique(values);

            this.select(select, values, true, e.type === "yChange");
        };

        select.addEventListener("change", change);
        select.addEventListener("yChange", change);

        if(select.ySelectParams.params.liveReload === true) {
            YTools.setDetectChangeHandler(select);

            // Listening changes by mutation observer
            const observer = new MutationObserver(mutationsList => {
                for (let mutation of mutationsList) {
                    switch (mutation.type) {
                        case "childList":

                            // Added elements
                            if (mutation.addedNodes.length) {
                                mutation.addedNodes.forEach(item => {
                                    if(item.tagName === "OPTION") {
                                        this.insertOption(
                                            select,
                                            item.innerText,
                                            item.value,
                                            {
                                                selected: item.selected,
                                                disabled: item.disabled,
                                            },
                                            item.index,
                                            true
                                        );
                                    } else {
                                        item.remove();
                                    }
                                });
                            }

                            // Removed elements
                            if (mutation.removedNodes.length) {
                                mutation.removedNodes.forEach(item => {
                                    if(item.tagName === "OPTION") {
                                        this.removeOption(select, item.index, true);
                                    }
                                });
                            }

                            break;
                        case "attributes":
                            if(select.ySelectParams.params.listenOptionAttrs.indexOf(mutation.attributeName) && mutation.target.tagName === "OPTION") {
                                let index = mutation.target._selectIndex,
                                    attributeValue = mutation.target.getAttribute(mutation.attributeName);

                                if(this.lockedListenOptionAttrs.indexOf(mutation.attributeName) >= 0) {
                                    select.ySelectParams.options[index].params[mutation.attributeName] = attributeValue !== null;
                                } else {
                                    select.ySelectParams.options[index].attributes[mutation.attributeName] = attributeValue;
                                }

                                this.draw(select);
                            }

                            break;
                    }
                }
            });

            observer.observe(select, this.observerParams);
            select.ySelectParams.observer = observer;
        }
    }
}

let ySelect = new YSelect();